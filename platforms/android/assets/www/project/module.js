define(function(require) {
    var v1 = require('project/domestic-list');
    var v2 = require('project/hot-list');
    var v3 = require('project/index');
    var v4 = require('project/newest-list');
    var v5 = require('project/recommend-list');
    var v6 = require('project/software-list');
    var v7 = require('project/software-detail-list');
    var v8 = require('project/software-tag-list');
    var v9 = require('project/software-detail');
    return {
        'domestic-list': v1,
        'hot-list': v2,
        'index': v3,
        'newest-list': v4,
        'recommend-list': v5,
        'software-list': v6,
        'software-detail-list': v7,
        'software-tag-list': v8,
        'software-detail': v9
    };
});