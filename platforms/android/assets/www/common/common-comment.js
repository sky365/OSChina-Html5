define(['text!../common/common-comment.html', "../base/openapi", '../base/util', '../base/components/footer-menu-detail'],
	function(viewTemplate, OpenAPI, Util, MenuDetail) {
		return Piece.View.extend({
			menu: null,
			events: {
				"click .tweetList": "goToReply",
				"click .homeBtn": "goBackToHome",
				"click .refreshBtn": "refreshCommentList"

			},
			refreshCommentList: function() {
				this.loadCommentList();
			},
			goBackToHome: function() {
				var from = Util.request("from");
				var checkDetail = Util.request("checkDetail");
				switch (checkDetail) {
					case "question/question-detail":
						{
							var module = "question/";
							break;
						}
					case "news/news-detail":
						{
							var module = "news/";
							break;
						}
					case "news/news-blog-detail":
						{
							var module = "news/";
							break;
						}
				}
				this.navigateModule(module + from, {
					trigger: true
				});
			},
			goToReply: function(el) {
				var $target = $(el.currentTarget);
				var tweetId = Util.request("id");
				var commentAuthorId = $target.attr("data-commentAuthorId");
				var commentId = $target.attr("data-commentId");

				var dataSource = "cube-list-comment-list";
				var fromType = Util.request("fromType");
				var catalog = 3;
				switch (fromType) {
					case "4":
						{
							catalog = 1;
							break;
						}
					case "3":
						{
							catalog = "blog";
							break;
						}
					case "2":
						{
							catalog = 2;
							break;
						}
				}
				this.navigateModule("common/comment-reply?tweetId=" + tweetId + "&commentAuthorId=" + commentAuthorId + "&commentId=" + commentId + "&dataSource=" + dataSource + "&catalog=" + catalog, {
					trigger: true
				});
			},
			render: function() {
				$(this.el).html(viewTemplate);

				Piece.View.prototype.render.call(this);
				return this;
			},
			onShow: function() {
				var me = this;
				var footerTemplate = $(me.el).find("#footerTemplate").html();

				var footerHtml = _.template(footerTemplate, {
					"id": Util.request("id"),
					"url": "common/common-comment",
					"activeIndex": 2
				});
				$("body").append(footerHtml);
				menu = new MenuDetail();
				var token = Piece.Store.loadObject("user_token");
				if (token !== null) {
					var accesstoken = token.access_token;
					var checkDetail = Util.request("checkDetail");
					switch (checkDetail) {
						case "news/news-detail":
							{
								var backdatail = "news_detail";
								break;
							}
						case "news/news-blog-detail":
							{
								var backdatail = "news_blog_detail";
								break;
							}
						case "question/question-detail":
							{
								var backdatail = "question_detail";
							}
							break;
					}
					Util.Ajax(OpenAPI[backdatail], "GET", {
						id: Util.request("id"),
						dataType: 'jsonp',
						access_token: accesstoken
					}, 'json', function(data, textStatus, jqXHR) {
						if (data) {
							//判断是否收藏
							var favorite = data.favorite;
							if (favorite === 1) {
								$(".tab-item").find('.collect').removeClass("icon-star-empty").addClass("icon-star");
								$('.icon-star').css("color", "#0882f0");
							}
							//判断评论数,新闻
							if (data.commentCount != 0) {
								$('.commentImg').css("display", "block");
								$('.commentCount').html(data.commentCount)
							}
							//判断评论数,问答
							if (data.answerCount != 0) {
								$('.commentImg').css("display", "block");
								$('.commentCount').html(data.answerCount)
							}

						}
					}, null, function(xhr, status) {
						me.loadCommentList();
					});
				} else {
					this.loadCommentList();
				}
			},
			gobackBtn: function() {
				window.history.back();
				
			},
			loadCommentList: function() {
				Util.loadList(this, 'comment-list', OpenAPI.comment_list, {
					'id': Util.request("id"),
					'catalog': Util.request("com"),
					'dataType': OpenAPI.dataType,
					'page': 1,
					'pageSize': OpenAPI.pageSize
				}, true);
			}
		}); //view define

	});