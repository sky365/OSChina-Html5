define(function(require) {
     var v1 = require('user/attention-list');
     var v2 = require('user/fans-list');
     var v3 = require('user/favorite-blog-list');
     var v4 = require('user/favorite-code-list');
     var v5 = require('user/favorite-news-list');
     var v6 = require('user/favorite-software-list');
     var v7 = require('user/favorite-topic-list');
     var v8 = require('user/index');
     var v9 = require('user/user-info');
     var v10 = require('user/systemSettings');
     var v11 = require('user/about-us');
     return {
         'attention-list': v1,
         'fans-list': v2,
         'favorite-blog-list': v3,
         'favorite-code-list': v4,
         'favorite-news-list': v5,
         'favorite-software-list': v6,
         'favorite-topic-list': v7,
         'index': v8,
         'user-info': v9,
         'systemSettings': v10,
         'about-us':v11
      };
});