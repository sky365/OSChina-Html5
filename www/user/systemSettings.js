define(['text!user/systemSettings.html', '../base/login/login', '../base/util', "../base/openapi", "../user/feedback"],
	function(viewTemplate, Login, Util, OpenAPI, Feedback) {
		return Piece.View.extend({
			id: 'user_systemSettings',
			events: {
				"click .backBtn": "goBackToHome",
				"click .clearCache": "clearCache",
				"click .UserLogin": "UserLogin",
				"click .myData": "goMyData",
				"click .feedback": "goFeedback",
				"click .aboutUs": "goAboutUs",
				"click #loadImg": "loadImg"
			},
			loadImg:function() {
				var isCheckd = $('#loadImg').is(":checked");
						if (isCheckd) {
							$('#loadImg').attr("checked", "true");
							Piece.Session.saveObject("loadImg",1);
						} else {
							$('#loadImg').attr("checked", "false");
							Piece.Session.saveObject("loadImg",0);
						}
			},
			goFeedback: function() {
				var maxHeigth = $(window).height() >= $(this.el).find("#scrollContent").height()? $(window).height():$(this.el).find("#scrollContent").height();
				var feedback = new Feedback()
				feedback.show(maxHeigth);
			},
			goMyData: function() {
				var checkLogin = Util.checkLogin();
				if (checkLogin === false) {
					login = new Login();
					login.show();
				} else {
					Backbone.history.navigate("user/user-info", {
						trigger: true
					});
				}
			},
			goBackToHome: function() {
				window.history.back();
			},
			goAboutUs:function() {
				Backbone.history.navigate("user/about-us", {
						trigger: true
					});
			},
			render: function() {
				$(this.el).html(viewTemplate);

				Piece.View.prototype.render.call(this);
				return this;
			},
			onShow: function() {
				
				
				
				// 判断是否登录
				var user_token = Piece.Store.loadObject("user_token");
				if (!user_token || user_token === null) {
					$('.UserLogin').html("用户登录")
				} else {
					$('.UserLogin').html("注销登录")
				}
				//判断是否加载图片
				var loadImg = Piece.Session.loadObject("loadImg");
				if(loadImg == "1") {
					$('#loadImg').attr("checked", "true");
				}

				// var myScroll = new iScroll('scrollContent', {
				// 	checkDOMChanges: true,
				// 	y: 10

				// });

				// var scrollHeight = document.body.scrollHeight;
				// var titHeight = $('h1').height();
				// var contentHeight = scrollHeight - titHeight;
				// $('#scrollContent').css("height", contentHeight + "px");
			},
			clearCache: function() {
				var userInformationToken = window.localStorage['user_token'];
				var userInformationMessage = window.localStorage['user_message'];
				var userInformationInfo = window.localStorage['user_info'];
				window.localStorage.clear();
				if (userInformationToken != null || userInformationToken != undefined) {
					window.localStorage["oscAppVersion"] = oscAppVersion;
					window.localStorage["user_token"] = userInformationToken;
					window.localStorage["user_message"] = userInformationMessage;
					window.localStorage["user_info"] = userInformationInfo;
				}
				new Piece.Toast('清除缓存成功');
			},
			UserLogin: function() {
				var loginInfor = $('.UserLogin').html()
				if (loginInfor === "用户登录") {
					login = new Login();
					login.show();
				} else {
					Piece.Store.deleteObject("user_token");
					Piece.Store.deleteObject("user_message");
					// Piece.Store.deleteObject("user_info");
					$('#footer-more-detail').hide();
					// 注销时清除我的空间所有列表的缓存
					Util.cleanStoreListData("my-atme-list");
					Util.cleanStoreListData("my-comment-list");
					Util.cleanStoreListData("my-critic-list");
					Util.cleanStoreListData("my-my-list");
					Util.cleanStoreListData("my-myself-list");
					$('.UserLogin').html("用户登录")
					new Piece.Toast("已注销登录");
				}

			}
		}); //view define

	});